python-oslo.messaging (16.1.0-2) experimental; urgency=medium

  * d/watch: switch to version=4 and mode=git.

 -- Thomas Goirand <zigo@debian.org>  Tue, 25 Feb 2025 15:19:31 +0100

python-oslo.messaging (16.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed build-depends for this release.
  * Rebased no-functional-test.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 24 Feb 2025 15:48:09 +0100

python-oslo.messaging (14.9.1-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090571).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 08:43:32 +0100

python-oslo.messaging (14.9.1-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 01 Nov 2024 13:28:55 +0100

python-oslo.messaging (14.9.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Sep 2024 17:54:17 +0200

python-oslo.messaging (14.9.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 27 Aug 2024 08:59:19 +0200

python-oslo.messaging (14.8.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed reproducible.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2024 16:51:21 +0200

python-oslo.messaging (14.7.0-3) unstable; urgency=medium

  * Make oslo.messaging, zaqar and magnum reproducible by adding this simple
    patch: reproducible.patch (Closes: #1068377).

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Apr 2024 12:00:31 +0200

python-oslo.messaging (14.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Apr 2024 16:36:04 +0200

python-oslo.messaging (14.7.0-1) experimental; urgency=medium

  * New upstream release.
  * Added python3-oslo.context as (b-)depends.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Feb 2024 21:59:24 +0100

python-oslo.messaging (14.4.1-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 09:15:46 +0200

python-oslo.messaging (14.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 13:32:47 +0200

python-oslo.messaging (14.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 31 Aug 2023 09:57:59 +0200

python-oslo.messaging (14.3.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Aug 2023 12:54:31 +0200

python-oslo.messaging (14.2.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1048050) and install doc correctly.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Aug 2023 15:48:46 +0200

python-oslo.messaging (14.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 11:31:21 +0200

python-oslo.messaging (14.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 10:48:36 +0100

python-oslo.messaging (14.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 13:49:53 +0200

python-oslo.messaging (14.0.0-1) experimental; urgency=medium

  * new upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Aug 2022 16:23:14 +0200

python-oslo.messaging (12.13.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add autopkgtest.
  * Blacklist RpcKombuHATestCase:
    - test_ensure_four_retry
    - test_ensure_no_retry
    - test_ensure_one_retry

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Mar 2022 16:06:11 +0100

python-oslo.messaging (12.13.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Feb 2022 17:50:10 +0100

python-oslo.messaging (12.12.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Feb 2022 11:47:08 +0100

python-oslo.messaging (12.9.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 16:42:44 +0200

python-oslo.messaging (12.9.1-1) experimental; urgency=medium

  * New upstream release.
  * Added python3-oslo.messaging as (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 27 Aug 2021 15:40:39 +0200

python-oslo.messaging (12.8.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Aug 2021 13:48:05 +0200

python-oslo.messaging (12.7.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 09:09:24 +0200

python-oslo.messaging (12.7.1-1) experimental; urgency=medium

  * New upstream release.
  * Remove --with systemd.
  * Debhelper 11.
  * Standards-Version: 4.5.1.

 -- Thomas Goirand <zigo@debian.org>  Mon, 15 Mar 2021 09:21:29 +0100

python-oslo.messaging (12.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed versions when satisfied in Bullseye.
  * Removed python3.9-fix-isAlive-is_alive.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Mar 2021 09:10:56 +0100

python-oslo.messaging (12.5.1-1) unstable; urgency=medium

  * New upstream version

 -- Michal Arbet <michal.arbet@ultimum.io>  Sun, 20 Dec 2020 10:30:34 +0100

python-oslo.messaging (12.5.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed d/watch.
  * Add a debian/salsa-ci.yml.
  * Add python3.9-fix-isAlive-is_alive.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 09:29:22 +0200

python-oslo.messaging (12.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Set python3-confluent-kafka min version to 1.3.0.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 11:27:42 +0200

python-oslo.messaging (12.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Rebased no-functional-test.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2020 21:07:06 +0200

python-oslo.messaging (12.1.3-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 27 Aug 2020 10:37:20 +0200

python-oslo.messaging (12.1.2-1) unstable; urgency=medium

  * New upstream release:
    - Fix testing with newer Kombu >= 4.6.8 (Closes: #966955).

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Aug 2020 11:54:14 +0200

python-oslo.messaging (12.1.1-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Jul 2020 11:25:45 +0200

python-oslo.messaging (12.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 22:10:41 +0200

python-oslo.messaging (12.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed add-support-for-kafka-ssl-autentication.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 15:25:54 +0200

python-oslo.messaging (10.2.0-3) unstable; urgency=medium

  [ Ondrej Burian ]
  * Add add-support-for-kafka-ssl-autentication.patch.

  [ Michal Arbet ]
  * Uploading with patch for kafka.
    Thanks to Ondrej Burian

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 04 Mar 2020 11:18:37 +0100

python-oslo.messaging (10.2.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 00:22:23 +0200

python-oslo.messaging (10.2.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Added python3-oslo.i18n as (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Sep 2019 13:13:00 +0200

python-oslo.messaging (9.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 01:29:58 +0200

python-oslo.messaging (9.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.
  * Removed oslo-messaging-zmq-receiver package.
  * Removed applied upstream:
    Use_ensure_connection_to_prevent_loss_of_connection_error_logs.patch

 -- Thomas Goirand <zigo@debian.org>  Wed, 20 Mar 2019 21:37:59 +0100

python-oslo.messaging (8.1.2-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Jan 2019 23:01:57 +0100

python-oslo.messaging (8.1.0-3) unstable; urgency=medium

  * Add Use_ensure_connection_to_prevent_loss_of_connection_error_logs.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 21 Nov 2018 15:49:06 +0100

python-oslo.messaging (8.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Sep 2018 23:08:41 +0200

python-oslo.messaging (8.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed all patches, all applied upstream.
  * Fixed (build-)depends for this release.
  * Rebased no-functional-test.patch.

 -- Thomas Goirand <zigo@debian.org>  Sun, 19 Aug 2018 23:18:20 +0200

python-oslo.messaging (5.35.0-3) unstable; urgency=medium

  [ Thomas Goirand ]
  * Remove all non-zmq tests blacklist.
  * Add 2 python 3.7 patches, thanks to James Page for them:
    - python3.7-deal-with-Exception-repr-changes.patch.
    - python3.7-do-not-use-async.patch.
  * Use team+openstack@tracker.debian.org as maintainer.
  * Add remove-test_zmq_address.py.patch.

  [ Michal Arbet ]
  * d/control: Bump debhelper
  * d/control: Add me to Uploaders field
  * d/control: Bump Standards version
  * d/control: Remove python-sphinx from build-dependencies
  * d/control: Add python3-sphinx to build-dependencies
  * d/control: Remove python-openstackdocstheme from build-dependencies
  * d/control: Add python3-openstackdocstheme to build-dependencies
  * d/control: Remove python pika libraries
  * d/rules: Switch generating docs to python3
  * d/copyright: Add me to copyright
  * d/patches: Remove remove-test_zmq_address.py.patch
  * d/patches: Add remove-zmq-tests.patch
  * d/patches: Add replace-raise-stop-iteration-with-return.patch
  * d/patches: Add remove-deprecated-pika-driver.patch

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 13 Jul 2018 13:48:28 +0100

python-oslo.messaging (5.35.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:24:42 +0000

python-oslo.messaging (5.35.0-1) experimental; urgency=medium

  * Set VCS URLs to point to salsa.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_test.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Sun, 11 Feb 2018 08:45:06 +0000

python-oslo.messaging (5.30.2-2) unstable; urgency=medium

  * Calls update-alternatives oslo-messaging-zmq-proxy to remove it from
    /usr/bin when upgrading from Stretch (Closes: #881774).
  * Do not run zeromq tests which are failing, and that probably absolutely
    nobody cares about.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Jan 2018 16:16:14 +0100

python-oslo.messaging (5.30.2-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 12 Dec 2017 08:12:28 +0000

python-oslo.messaging (5.30.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Switched to debhelper 10.
  * Missing runtime lsb-base depends.
  * Standards-Version is now 4.1.1.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Nov 2017 22:20:34 +0000

python-oslo.messaging (5.30.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Rebased patches, removed git call in doc fix patch (not needed now).
  * Removed now useless transition package.
  * Blacklist RpcKombuHATestCase tests (broken).
  * Fixed alternatives in /usr/bin.

  [ Daniel Baumann ]
  * Updating standards version to 4.0.1.
  * Deprecating priority extra as per policy 4.0.1.
  * Updating standards version to 4.1.0.

 -- Thomas Goirand <zigo@debian.org>  Sat, 05 Aug 2017 18:04:39 +0200

python-oslo.messaging (4.6.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 09:16:49 +0000

python-oslo.messaging (4.6.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 02 Apr 2016 14:30:34 +0200

python-oslo.messaging (4.5.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Mar 2016 10:56:14 +0100

python-oslo.messaging (4.5.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Fixed debian/copyright order.
  * Standards-Version: 3.9.7 (no change).
  * Added remove-git-log-call-in-sphinx-doc.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Mar 2016 12:59:50 +0000

python-oslo.messaging (4.0.0-1) experimental; urgency=medium

  [ Corey Bryant ]
  * d/control: Set minimum kombu version to 3.0.30 due to:
    https://github.com/celery/kombu/issues/545.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed MANIFEST.in patches.
  * Run Python 3 tests in python 3.5 as well.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Jan 2016 02:43:30 +0000

python-oslo.messaging (3.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed do-not-run-toplevel-test.patch.
  * Upstream has renamed oslo-messaging-zmq-receiver into
    oslo-messaging-zmq-broker, fixing the package to follow on.

 -- Thomas Goirand <zigo@debian.org>  Tue, 01 Dec 2015 12:08:25 +0100

python-oslo.messaging (2.5.0-2) unstable; urgency=medium

  * override_dh_python3 to fix Py3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Oct 2015 23:26:32 +0000

python-oslo.messaging (2.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * python-oslo-messaging transition package in priority extra.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 20:07:13 +0000

python-oslo.messaging (2.1.0-2) experimental; urgency=medium

  * Override dh_auto_clean to make sure it doesn't fetch pbr.
  * Disable some test which are too fragile (ie: timing issues):
    - drivers.test_impl_rabbit.TestSendReceive.test_send_receive
    - drivers.test_impl_rabbit.TestSendReceive.test_send_receive

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Sep 2015 11:27:55 +0200

python-oslo.messaging (2.1.0-1) experimental; urgency=medium

  [ James Page ]
  * d/p/use-mox3.patch: Cherry pick fix from upstream VCS to directly
    use mox3 instead of using an import of mox from six.moves, resolving
    FTBFS due to changes in oslotest.
  * Fixup typo in transitional package description (LP: #1471561).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed upstream VCS url.
  * Fixed (build-)depends for this release.
  * Blacklist 3 failed unit tests.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jul 2015 09:19:43 +0000

python-oslo.messaging (1.14.0-1) experimental; urgency=medium

  [ James Page ]
  * Team upload.
  * New upstream release.

  [ Thomas Goirand ]
  * Make the build reproducible. Thanks to Juan Picca <jumapico@gmail.com> for
    the bug report and patch (Closes: #788480).
  * Added python3 support.
  * Added oslo-messaging-zmq-receiver.init sysv-rc script.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Jun 2015 23:37:49 +0200

python-oslo.messaging (1.13.0-1) experimental; urgency=medium

  * New upstream release:
    - d/control: Align version requirements and dependencies with upstream.
  * Add myself to Uploaders.
  * Ensure that unit test failures result in a package build failure.
  * Add new binary package for oslo-messaging-zmq-receiver.
  * Explicitly install python to python-oslo.messaging.
  * Rename source package to python-oslo.messaging.
  * Re-align with Ubuntu:
    - d/control: Add Breaks/Replaces and transitional package for
      python-oslo-messaging in Ubuntu.
  * d/*: wrap-and-sort.

 -- James Page <james.page@ubuntu.com>  Tue, 09 Jun 2015 08:52:35 +0100

oslo.messaging (1.8.1-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed (build-)depends for this release.
  * Standards-Version: bump to 3.9.6 (no change).

 -- Thomas Goirand <zigo@debian.org>  Thu, 16 Apr 2015 09:21:46 +0200

oslo.messaging (1.4.1-2) experimental; urgency=medium

  * Added remove-PROTOCOL_SSLv3.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 21 Nov 2014 10:01:03 +0000

oslo.messaging (1.4.1-1) experimental; urgency=medium

  * New upstream release, which is in fact the same as 1.4.0, though this fixes
    the mess with version numbers. Thanks a lot to Doug Hellmann for it.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Sep 2014 15:12:40 +0000

oslo.messaging (1.4.0.0-2) experimental; urgency=medium

  * Uploading to experimental. Version 1.4 should never have been uploaded
    to unstable, so repairing this.

 -- Thomas Goirand <zigo@debian.org>  Sun, 21 Sep 2014 16:01:44 +0000

oslo.messaging (1.4.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Note that this really is 1.4.0, though because of previous upload to
    Debian using 1.4.0.0~a4 (which should have been 1.4.0~a4), then we do need
    to use that version number.
  * Updated (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 21 Sep 2014 22:26:34 +0800

oslo.messaging (1.4.0.0~a4-1) unstable; urgency=medium

  * New upstream release.
  * Updated required version for python{3,}-six.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 Aug 2014 19:09:30 +0800

oslo.messaging (1.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Apr 2014 20:10:04 +0800

oslo.messaging (1.3.0~a9-1) experimental; urgency=medium

  * New upstream release.
  * Standards-Version: is now 3.9.5.
  * Updated (build-)dependency.
  * Better usage of testr/subunit.
  * Refreshed no-intersphinx.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 21 Mar 2014 17:10:55 +0800

oslo.messaging (1.3.0~a5-1) unstable; urgency=medium

  * New upstream release.
  * Fixed sphinxdoc build.
  * Fixed all (build-)depends for the new version.

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Jan 2014 15:10:21 +0800

oslo.messaging (1.2.0~a11-2) unstable; urgency=low

  * Added missing build-depends: openstack-pkg-tools (Closes: #724090).

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Sep 2013 10:03:12 +0800

oslo.messaging (1.2.0~a11-1) unstable; urgency=low

  * Initial release (Closes: #721511).

 -- Thomas Goirand <zigo@debian.org>  Sun, 01 Sep 2013 21:27:09 +0800
